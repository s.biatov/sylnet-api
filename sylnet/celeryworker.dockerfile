FROM python:3.6.5-slim

RUN mkdir -p /app/sylnet/
COPY ./sylnet/requirements.txt /app/sylnet/
RUN pip install --upgrade pip && \
    pip install -r /app/sylnet/requirements.txt

WORKDIR /app/
COPY . /app

ENV PYTHONPATH=/app

import os
from functools import lru_cache
from typing import Any, Dict, List, Optional, Union

from pydantic import BaseSettings, PostgresDsn, validator


def assemble_value(v: Union[str, List[str]]) -> Union[List[str], str]:
    if isinstance(v, str) and not v.startswith("["):
        return [i.strip() for i in v.split(",")]
    elif isinstance(v, (list, str)):
        return v
    raise ValueError(v)


class Settings(BaseSettings):
    ENV: str = "staging"

    POSTGRES_HOSTNAME: str
    POSTGRES_PORT: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DB: str

    SQLALCHEMY_DATABASE_URI: Optional[str]
    DATABASE_ENGINE_POOL_SIZE: Optional[int] = 20
    DATABASE_ENGINE_MAX_OVERFLOW: Optional[int] = 0

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        port = values.get('POSTGRES_PORT') or 5432
        return PostgresDsn.build(
            scheme="postgresql+psycopg2",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_HOSTNAME"),
            port=str(port),
            path=f"/{values.get('POSTGRES_DB') or ''}",
        )

    FILE_OUT_PATH: str = os.path.abspath("sylnet/config_files")

    CELERY_TASK_TRACK_STARTED: bool = True
    CELERY_MAIN_QUEUE: str = "main-queue"
    CELERY_BROKER_URL: str = "amqp://guest:guest@rabbitmq:5672//"
    CELERY_ACCEPT_CONTENT: Union[List[str], str] = ["application/json"]
    CELERY_TASK_SERIALIZER: str = "json"
    CREATE_TASKS: bool = True

    _assemble_accept_content = validator("CELERY_ACCEPT_CONTENT", allow_reuse=True)(assemble_value)

    class Config:
        case_sensitive = True
        env_file = ".env"


@lru_cache(maxsize=128)
def get_settings():
    return Settings()


settings = get_settings()

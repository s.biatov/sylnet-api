from sylnet.core.celery import celery_app
from sylnet.core.config import settings
from sylnet import utils


@celery_app.task(name="run", queue=settings.CELERY_MAIN_QUEUE)
def run(path: str, content: str, version: str) -> None:
    utils.run_process(path=path, content=content, version=version)

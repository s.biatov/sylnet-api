import base64
import errno
import os
import subprocess
from typing import Optional

from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker, Session

from sylnet.core.config import settings

engine = create_engine(
    settings.SQLALCHEMY_DATABASE_URI,
    pool_size=settings.DATABASE_ENGINE_POOL_SIZE,
    max_overflow=settings.DATABASE_ENGINE_MAX_OVERFLOW,
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_db() -> Session:
    try:
        db = SessionLocal()
        return db
    except Exception as e:
        print(e)
        db.rollback()
    finally:
        db.close()


def get_main_file_path():
    return os.path.dirname(os.path.realpath(__file__))


def upload_file(path: str, content: str) -> None:
    check_file_path(path)
    bytes_content = encode_content(content)
    try:
        with open(path, "wb") as file:
            file.write(bytes_content)
    except (FileNotFoundError, OSError):
        return None


def encode_content(content: str) -> bytes:
    byte_data = content.encode()
    return base64.b64decode(byte_data)


def check_file_path(path: str) -> None:
    if os.path.exists(path):
        return None
    try:
        os.makedirs(os.path.dirname(path))
    except FileNotFoundError:
        return None
    except OSError as exc:  # Guard against race condition
        if exc.errno != errno.EEXIST:
            raise exc


def run_process(path: str, content: str, version: str) -> None:
    upload_file(path=path, content=content)
    abs_path = os.path.abspath("sylnet")
    subprocess.call(["python", f"{abs_path}/versions/{version}/run_SylNet.py", path])


def update_text_syllable(path: str, value: Optional[float] = None):
    db = get_db()
    db.execute(
        text("UPDATE file SET syllable_count=:syllable_count WHERE path=:path"),
        [{"path": path, "syllable_count": value}]
    )
    db.commit()

FROM python:3.9-slim

RUN mkdir -p /app/backend/
COPY ./backend/requirements.txt /app/backend/
RUN pip install --upgrade pip && \
    pip install -r /app/backend/requirements.txt

WORKDIR /app/
COPY . /app

ENV PYTHONPATH=/app
EXPOSE 8080

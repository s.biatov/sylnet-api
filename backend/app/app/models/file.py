from sqlalchemy import Column, Integer, String, Float
from backend.app.app.db.base_class import Base


class File(Base):
    id = Column(String, primary_key=True, index=True)
    name = Column(String, nullable=False, index=True)
    content_type = Column(String, nullable=True)
    extension = Column(String, nullable=False)
    path = Column(String, nullable=False, index=True)
    syllable_count = Column(Float, nullable=True)

import enum
from typing import Optional

from pydantic import BaseModel


class FuncNameEnum(str, enum.Enum):
    run = "run"


class SylNetVersionEnum(str, enum.Enum):
    v1 = "v1"
    # v2 = "v2"  # TODO: add version2


class FileBase(BaseModel):
    name: Optional[str] = None
    content_type: Optional[str] = None
    extension: Optional[str] = None
    syllable_count: Optional[float] = None


class FileCreate(FileBase):
    id: str
    name: str
    content_type: str
    extension: str
    path: str


class FileUpdate(FileBase):
    pass


class FileInDBInitial(BaseModel):
    id: str


class FileInDBBase(FileInDBInitial, FileBase):
    name: str
    content_type: str
    extension: str

    class Config:
        orm_mode = True


# Properties to return to client
class File(FileInDBBase):
    pass


# Properties stored in DB
class FileInDB(FileInDBBase):
    pass

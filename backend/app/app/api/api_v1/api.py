from fastapi import APIRouter

from backend.app.app.api.api_v1.endpoints import files

api_router = APIRouter()
api_router.include_router(files.router, prefix="/files", tags=["files"])

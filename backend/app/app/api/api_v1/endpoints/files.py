from typing import Any, Optional

from fastapi import APIRouter, Depends, HTTPException, UploadFile
from sqlalchemy.orm import Session

from backend.app.app import crud, schemas
from backend.app.app.db.session import get_db
from backend.app.app.api import deps, services

router = APIRouter()


@router.post(
    "/",
    response_model=schemas.FileInDBInitial,
    dependencies=[Depends(deps.check_files_path)],
)
async def upload_file(
        db: Session = Depends(get_db),
        file: UploadFile = Depends(deps.validate_content_type),
        func: Optional[schemas.FuncNameEnum] = schemas.FuncNameEnum.run,
        version: Optional[schemas.SylNetVersionEnum] = schemas.SylNetVersionEnum.v1,
):
    """
    Upload file.
    """
    file_db = services.save_file(db=db, file=file, func=func, version=version)
    return file_db


@router.get("/{id}", response_model=schemas.File)
def read_file(
    *,
    db: Session = Depends(get_db),
    id: str,
) -> Any:
    """
    Get file by ID.
    """
    file = crud.file.get(db=db, id=id)
    if not file:
        raise HTTPException(status_code=404, detail="File not found")
    return file

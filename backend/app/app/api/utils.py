import base64
import uuid

from fastapi import UploadFile
from sqlalchemy.orm import Session

from backend.app.app.core.config import settings
from backend.app.app import crud, models


def generate_internal_name() -> str:
    return str(uuid.uuid4())


def get_file_extension(file_name: str) -> str:
    return file_name.rsplit(".", 1)[-1]


def get_file_path(db: Session, file_id: str) -> str:
    file_db: models.File = crud.file.get(db=db, id=file_id)
    if not file_db:
        return ""
    return generate_file_path(file_id=file_id, extension=file_db.extension)


def generate_file_path(file_id: str, extension: str) -> str:
    return f"{settings.FILE_OUT_PATH}/{settings.ENV}/{file_id}.{extension}"


def get_content_file(file: UploadFile) -> str:
    bytes_content = file.file.read()
    return base64.b64encode(bytes_content).decode()

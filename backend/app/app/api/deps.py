from fastapi import UploadFile, File, HTTPException, status

from backend.app.app.core.config import settings
from sylnet.utils import check_file_path


def check_files_path() -> None:
    check_file_path(path=settings.FILE_OUT_PATH)


def validate_content_type(file: UploadFile = File()) -> UploadFile:
    content_types = settings.FILE_CONTENT_TYPES
    if content_types and file.content_type not in content_types:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"File type of '{file.content_type}' is not supported.",
        )
    return file

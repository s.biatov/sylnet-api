from typing import Optional

from fastapi import UploadFile
from sqlalchemy.orm import Session

from backend.app.app import crud, schemas
from backend.app.app.api import utils
from backend.app.app.core.celery import celery_app
from backend.app.app.core.config import settings


def save_file(
        db: Session,
        file: UploadFile,
        func: Optional[schemas.FuncNameEnum] = schemas.FuncNameEnum.run,
        version: Optional[schemas.SylNetVersionEnum] = schemas.SylNetVersionEnum.v1,
) -> schemas.FileInDBInitial:
    internal_name = utils.generate_internal_name()
    extension = utils.get_file_extension(file.filename)
    path = utils.generate_file_path(file_id=internal_name, extension=extension)
    file_in = schemas.FileCreate(
        id=internal_name,
        name=file.filename,
        content_type=file.content_type,
        extension=extension,
        path=path,
    )
    file_db = crud.file.create(db=db, obj_in=file_in)

    content = utils.get_content_file(file)
    celery_app.send_task(
        func.value,
        kwargs=dict(path=file_db.path, content=content, version=version.value),
        queue=settings.CELERY_MAIN_QUEUE,
    )
    return schemas.FileInDBInitial(**file_in.dict())

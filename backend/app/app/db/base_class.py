import re
from typing import Any

from sqlalchemy.ext.declarative import as_declarative, declared_attr


def resolve_table_name(name):
    """Resolves table names to their mapped names."""
    names = re.split("(?=[A-Z])", name)  # noqa
    return "_".join([x.lower() for x in names if x])


@as_declarative()
class Base:
    id: Any

    __name__: str

    @declared_attr
    def __tablename__(self) -> str:
        return resolve_table_name(self.__name__)

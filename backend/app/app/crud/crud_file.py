from backend.app.app.crud.base import CRUDBase
from backend.app.app.models.file import File
from backend.app.app.schemas.file import FileCreate, FileUpdate


class CRUDFile(CRUDBase[File, FileCreate, FileUpdate]):
    pass


file = CRUDFile(File)

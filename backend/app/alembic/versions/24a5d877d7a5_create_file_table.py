"""create file table

Revision ID: 24a5d877d7a5
Revises: 
Create Date: 2022-07-13 09:43:07.249561

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '24a5d877d7a5'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "file",
        sa.Column("id", sa.String(), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.Column("content_type", sa.String(), nullable=True),
        sa.Column("extension", sa.String(), nullable=False),
        sa.Column("syllable_count", sa.Float(), nullable=True),
        sa.Column("path", sa.String(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_file_id"), "file", ["id"], unique=True)
    op.create_index(op.f("ix_file_name"), "file", ["name"], unique=False)
    op.create_index(op.f("ix_file_path"), "file", ["path"], unique=True)


def downgrade() -> None:
    pass

#! /usr/bin/env bash
set -e

cd /app/backend/app
/usr/local/bin/alembic upgrade head
/usr/local/bin/gunicorn -c /app/backend/app/gunicorn_conf.py backend.app.app.main:app

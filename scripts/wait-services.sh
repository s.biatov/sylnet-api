#! /usr/bin/env bash

set -ue

: "${PING:=60}"

while true; do
  echo 'ping'
  echo -n
  sleep ${PING}
done

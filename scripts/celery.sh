#! /usr/bin/env bash
set -e

celery -A sylnet.core.celery worker -l info -Q "${CELERY_MAIN_QUEUE}" -c 1
